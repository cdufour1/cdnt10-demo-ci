const encrypt = require('../utils');
const assert = require('assert'); // module natif de node

describe('Utilitaires', () => {

  it('should encrypt the password', () => {

    var clearPwd = "Zorro-tata_47-PM";
    var encrypted = encrypt(clearPwd);

    assert.strictEqual(encrypted, "Morro-tata_47-PZ");
  })

})