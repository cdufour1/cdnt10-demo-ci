/**
 * 
 * La fonction encript inverse le premier et le dernier caractère
 * du mot de passe fourni en entrée
 */
function encrypt(clearPassword) {
  if (clearPassword.length < 8) return false;

  var firstChar = clearPassword[0];
  var lastChar = clearPassword[clearPassword.length - 1];
  var middle = clearPassword.substr(1, clearPassword.length - 2);
  
  return lastChar + middle + firstChar;

}

module.exports = encrypt;