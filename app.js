const express = require('express');
const app = express();
const port = 7000;

app.get('/', (req, res) => {
  res.send('Home');
})

// app.post('/user', (req, res) => {
//   // ToDO: récupérer et traiter les données de création d'un compte utilisateur
// })

app.listen(port, () => {
  console.log('Serveur écoutant le port ' + port);
})
